FROM gitpod/workspace-base:latest

RUN curl -fsSL https://get.deta.dev/space-cli.sh | sh
RUN mv /home/gitpod/.detaspace /workspace/
RUN export PATH="/workspace/.detaspace/bin:$PATH"
