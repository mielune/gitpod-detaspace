# Gitpod workspace to build Deta Space apps

- [Gitpod](https://www.gitpod.io/)
- [Deta Space](https://deta.space/)

# Deta Space setup

See [Deta Space documentation](https://deta.space/docs)

You can create an access token under Settings in your Space. Just type ‘settings’ in Teletype to open them.


